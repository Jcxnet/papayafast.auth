<!DOCTYPE html>
<html>
    <head>
      <title>Papayafast Auth v1</title>
      <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="text-effect">Papayafast Auth v1</div>
            </div>
        </div>
    </body>
</html>
