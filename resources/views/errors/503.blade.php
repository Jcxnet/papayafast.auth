<!DOCTYPE html>
<html>
    <head>
      <title>Papayafast Auth v1 - 503</title>
      <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
      <link rel="stylesheet" href="{{ secure_asset('css/style.css') }}" />
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="text-effect">Error 503</div>
            </div>
        </div>
    </body>
</html>