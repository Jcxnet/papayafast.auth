FORMAT: 1A

# ApidocTest

# AppApiv1ControllersUserController

## Registra un usuario en la base de datos, verifica que no exista el email [POST /]


+ Request (application/json)
    + Body

            {
                "name": "nombre",
                "email": "email@server.com",
                "password": "clave"
            }

+ Response 200 (application/json)
    + Body

            {
                "status": "ok",
                "data": {
                    "name": "nombre",
                    "email": "email@server.com",
                    "verify_token": "7asgfjws3das"
                }
            }