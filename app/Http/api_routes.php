<?php

$api = app('Dingo\Api\Routing\Router');

$api->version('v1', function ($api) {

	$api->post('login', 		['as' => 'auth.login',		'uses' => 'App\Api\v1\Controllers\UsersController@login'] );
	$api->post('logout', 		['as' => 'auth.logout',		'uses' => 'App\Api\v1\Controllers\UsersController@logout'] );
	$api->post('register', 	['as' => 'auth.register',	'uses' => 'App\Api\v1\Controllers\UsersController@register'] );
	$api->post('recovery', 	['as' => 'auth.recovery',	'uses' => 'App\Api\v1\Controllers\UsersController@recovery'] );
	$api->post('reset', 		['as' => 'auth.reset',		'uses' => 'App\Api\v1\Controllers\UsersController@reset'] );
	$api->post('update', 		['as' => 'auth.update',		'uses' => 'App\Api\v1\Controllers\UsersController@update'] );
	$api->post('info', 			['as' => 'auth.info',			'uses' => 'App\Api\v1\Controllers\UsersController@info'] );

	// example of protected route
	$api->get('protected', ['middleware' => ['api.auth'], function () {
		return \App\User::all();
    }]);

	// example of free route
	$api->get('free', function() {
		return \App\User::all();
	});

});