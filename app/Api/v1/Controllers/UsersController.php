<?php

namespace App\Api\v1\Controllers;

use JWTAuth;
use Validator;
use Config;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Password;
use Tymon\JWTAuth\Exceptions\JWTException;
use DB;

use App\Api\v1\Transformers\UserTransformer;

class UsersController extends Controller
{

    /**
     * Registra un usuario en la base de datos, verifica que no exista el email
     * @param  Request $request
     * @return json
     */
    public function register(Request $request){
  	  $registerFields	= ['name','email','password'];
      $userData 			= $request->only($registerFields);
      $validator = Validator::make($userData, [
       	'name'     => 'required|min:4|max:255|alpha_spaces',
       	'email'    => 'required|email|max:255|unique:users,email',
       	'password' => 'required|min:6|max:255'
    	]);
      if($validator->fails()) {
          return $this->response->error(implode(',',$validator->errors()->all()), 202);
      }
      $faker = \Faker\Factory::create();
      User::unguard();
      $userData['uuid'] = $faker->uuid();
      $userData['verify_token'] = str_random(32);
      $user = User::create($userData);
      User::reguard();
      if(!$user->id) {
          return $this->response->error('No se pudo registrar el usuario', 202);
      }
      //return $this->login($request);
      return response()->json(['meta'=>['status'=>'ok'], 'data' => collect($userData)->except(['uuid','password'])->toArray() ]);
    }

    /**
     * Verifica los datos de registro de un usuario y retorna el token JWT generado
     * @param  Request $request
     * @return json
     */
    public function login(Request $request){
    	$credentials = $request->only(['email', 'password']);
      $validator = Validator::make($credentials, [
          'email' => 'required|email',
          'password' => 'required',
      ]);
      if($validator->fails()) {
          return $this->response->error(implode(',',$validator->errors()->all()), 202);
      }
      try {
          if (! $token = JWTAuth::attempt($credentials)) {
              return $this->response->errorUnauthorized('Verifique su nombre de usuario y contraseña');
          }
      } catch (JWTException $e) {
          return $this->response->error('No se pudo crear el token de verificación', 202);
      }
      $uuid = JWTAuth::getPayload($token)->get('sub');
      return response()->json(['meta'=>['status'=>'ok'],'data' => ['token' =>$token ] ]);
    }

    /**
     * Invalida el token de un usuario
     * @param  Request $request
     * @return json
     */

    public function logout(Request $request){
	    if (! $user = JWTAuth::parseToken()->toUser()){
	    		return $this->response->errorUnauthorized('Usuario no autorizado');
	    	}
	    try{
	    	$token = JWTAuth::getToken($request);
	    	JWTAuth::invalidate($token);
	    	return response()->json(['meta'=>['status'=>'ok']]);
	    }catch(JWTException $e){
	    	return $this->response->error($e->getMessage(), 202);
	    }
    }

    /**
     * Crea un token para resetear la clave de un usuario, verifica si el email existe
     * @param  Request $request
     * @return json
     */
    public function recovery(Request $request){
    	$email = $request->only(['email']);
      $validator = Validator::make($email, [
          'email' => 'required|email',
      ]);
      if($validator->fails()) {
      	return $this->response->error(implode(',',$validator->errors()->all()), 202);
      }
      try {
          if (! $user = User::where('email',$email)->first()) {
              return $this->response->errorUnauthorized('Usuario no registrado');
          }
      } catch (Exception $e) {
          return $this->response->error('Ocurrió un error inesperado', 202);
      }
      $userToken = Password::createToken($user);
      try{
        $code = strtoupper(str_random(6));
        $dbToken = DB::table('password_resets')->where('email',$email)->where('token',$userToken)->update(['code'=>$code]);
      }catch(Exception $e){
        return $this->response->error('No se pudo crear el token', 202);
      }
      return response()->json(['meta'=>['status'=>'ok'], 'data'=>['name'=>$user->name,'email'=>$email['email'],'token'=>$userToken,'code'=>$code]]);
    }

    /**
     * Cambia la calve de un usuario en base el token y al código enviado por email
     * @param  Request $request
     * @return json
     */
    public function reset(Request $request){
    	$credentials = $request->only(['email', 'password', 'token', 'code','password_confirmation']);
      $validator = Validator::make($credentials, [
         'token'    => 'required',
         'code'     => 'required|string|size:6',
         'email'    => 'required|email',
         'password' => 'required|confirmed|min:6|max:255',
      ]);
      if($validator->fails()) {
          return $this->response->error(implode(',',$validator->errors()->all()), 202);
      }
      try{
        if(!$dbToken = DB::table('password_resets')->select(['created_at'])->where('code',$request->code)->where('token',$request->token)->first()){
          return $this->response->error('El código es incorrecto o no existe', 202);
        }
        $expire = strtotime($dbToken->created_at) + ( config('auth.passwords.users.expire') * 60);
        if($expire < time()){
          return $this->response->error('El código ha expirado', 202);
        }
        if( $result = Password::reset( $request->except(['code']),function($user,$pass){
          User::unguard();
          $user->update(['password'=>$pass]);
          User::reguard();
         }) == Password::PASSWORD_RESET ){
          return response()->json(['meta'=>['status'=>'ok']]);
        }
        return $this->response->error('No se pudo cambiar la clave', 202);
      }catch(Exception $e){
        return $this->response->error($e->getMessage(), 202);
      }
    }

    /**
     * actualiza email, imagen y nombre del usuario, necesita el token para verificar el usuario actual
     * @param  Request $request
     * @return json
     */
    public function update(Request $request){
    	try{
	    	if (! $user = JWTAuth::parseToken()->toUser()){
	    		return $this->response->errorUnauthorized('Usuario no autorizado');
	    	}
	    	$validator = Validator::make($request->all(), [
        	'name'  => 'min:4|max:255|alpha_spaces',
        	'email' => 'email|max:255|unique:users,email,'.$user->id,
        	'image'	=> 'string|max:255',
	      ]);
	      if ($validator->fails()) {
	        return $this->response->error(implode(',',$validator->errors()->all()), 202);
	      }
	      $data = [];
	      if(isset($request->name))
	      	$data['name'] = $request->name;
	      if(isset($request->email))
	      	$data['email'] = $request->email;
	      if(isset($request->image))
	      	$data['image'] = $request->image;
	      try{
	      	$user->update($data);
	      	return $this->response->item($user, new UserTransformer)->addMeta('status','ok');
	      }catch(Exception $e){
	      	return $this->response->error($e->getMessage(), 202);
	      }
	    }catch(JWTException $e){
	    	return $this->response->error($e->getMessage(), 202);
	    }
    }

    /**
     * Busca y retorna la información del usuario
     * @param  Request $request
     * @return json
     */
    public function info(Request $request){
    	try{
    		if (! $user = JWTAuth::parseToken()->toUser()){
    			return $this->response->errorUnauthorized('Usuario no autorizado');
    		}
    		return $this->response->item($user, new UserTransformer)->addMeta('status','ok');
    	}catch(JWTException $e){
    		return $this->response->error($e->getMessage(), 202);
    	}
    }
}