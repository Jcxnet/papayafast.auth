<?php

namespace App\Api\v1\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract{

	public function transform(User $user){
		return [
			'id' 		=> $user->uuid,
			'name' 	=> $user->name,
			'email'	=> $user->email,
			'image' => $user->image,
		];
	}

}
