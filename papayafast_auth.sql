-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-01-2017 a las 11:30:38
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `papayafast.auth`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `code` varchar(6) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `code`, `created_at`) VALUES
('jcxnet@gmail.com', 'f24e364c5e040dad98d67ff5d1eb531052e844733f144a71f494b750f820d92b', 'B6JPPZ', '2016-05-03 17:39:47'),
('juancarlos.iberico@cinepapaya.com', 'f28e9e8578ac2a9003423d7f43a8302e27fb88aaeaca94c80658b8ce99e16faf', 'A6M2AE', '2016-08-19 16:06:08'),
('franco@gmail.com', '3da7b1d90693b8122c2aa0f75f7058adcce98d265823bc609cc2fb603ab2e987', '4DEMHJ', '2016-12-12 16:59:25');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `uuid` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `verify_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `verified_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `uuid`, `name`, `email`, `password`, `token`, `remember_token`, `verify_token`, `image`, `created_at`, `updated_at`, `verified_at`, `deleted_at`) VALUES
(6, 'cceb757c-4831-32fe-aabf-45114a813bf7', 'Juan Carlos', 'jcxnetd@gmail.com', '$2y$10$B9dU7aSuRgP5eY5xqMMDY.RVWHxyPna4fs9yt/cMvR4qPm7UL4jGW', NULL, NULL, 'TPhUcrqxE6w3y1FFLdn5zxpcloRhEUAa', NULL, '2016-05-03 00:50:56', '2016-05-03 00:50:56', NULL, NULL),
(12, 'f512bcc4-bd92-3985-8dfb-c1b5208c77b7', 'test user', 'user@server.com', '$2y$10$ZLjVOiW5lyoPl4FssSWSF.so0w0k8Khezjz0gGa0H48/QqMulU672', NULL, NULL, 'PlVAu6S30QvueMffeuS2djRcNBTZPlQ6', NULL, '2016-05-03 01:26:33', '2016-08-20 00:23:33', NULL, NULL),
(41, 'e42a05ce-6f6f-3de4-b78f-da79b551f1f7', 'Juan Carlos', 'juancarlos.iberico@cinepapaya.com', '$2y$10$yauPVwUMxqsoHnNcRP2Kte9jcsoEDWC1ffS0w6MIWTFSR1az829Ba', NULL, NULL, '0cdwmivmeF8vChccQHNDaZsFMIt5KFCc', 'http://192.168.11.182/api.papayafast.com/public/images/users/3e43682f-88d1-3f0a-beba-228c217d0a4f.png', '2016-05-17 20:31:18', '2016-12-09 21:46:01', NULL, NULL),
(42, 'd7859507-eea6-3cb3-8ad9-7fdf628a4ba6', 'user demo', 'c3353564@trbvn.com', '$2y$10$V1alD2RjGlusXKwfTJ0I0eQ4rmA3bbK1sSIcBdI39QvT68LIhDHbu', NULL, NULL, 'h9SiMPCONawGmTreDTPKUoI8GzqBt4uX', NULL, '2016-06-02 19:21:03', '2016-06-02 19:21:03', NULL, NULL),
(43, '866a1b40-44c0-31dd-b04c-bf7357bcb5ef', 'User FF', 'test@extremail.ru', '$2y$10$1UEYwQ6NlzL6fga02MWEf.GKUTyTtEwJY9ugrtqQKJZwG3O6wr/1W', NULL, NULL, 'oYeMJz9nhsHGl0fH7kzzdt2rrdOIFjTP', NULL, '2016-06-03 00:12:40', '2016-06-03 00:12:40', NULL, NULL),
(44, 'ccbfe260-d5d8-363e-89df-cfdda4a50671', 'José Luis', 'jcxnets@gmail.com', '$2y$10$/4MXIBA6Xg65shllhLKTAuwQ2jolZVKONjUW22cri6wnQBEa.Ohdi', NULL, NULL, 'I7nkShr9BNApKtdr1Vw7aa7PZ6zirZF3', 'http://api.localhost/images/users/6d2c5707-4a4d-3fd3-9662-a1b859d7135a.jpeg', '2016-06-03 02:01:43', '2016-06-10 20:31:29', NULL, NULL),
(45, '3bf5cf53-ce7c-319a-aaa4-7282f0c69c8e', 'user demo', 'jcxnet@gmail.com', '$2y$10$wwe959MQ0cfC4cWDqR5XN.iKtjQy.4yHyzKynMe9.qbg.d30CaFLe', NULL, NULL, 'JVuh1YFitwnbOL3ibuNeB0K5E2BNhbee', 'http://api.localhost/images/users/e6d79d09-5407-33f4-ae45-3a1336c2a03c.jpeg', '2016-06-15 02:32:07', '2016-07-06 21:46:36', NULL, NULL),
(46, '3d567461-5d4b-3ce1-915a-db0c631f7c41', 'Alfredo', 'alfredo@cinepapaya.com', '$2y$10$wtH5XeYblypC8AFgti5FiefTXImHxrj5hzFouOjUMCeNSp9/I9j8i', NULL, NULL, 'WHKOrieLnwVHXrQArs5mwhWbRvxnbv3L', NULL, '2016-07-07 03:01:25', '2016-07-07 03:01:25', NULL, NULL),
(50, '61030009-79b4-3563-9e62-72904943ea1b', 'Invitado', 'f465196@mvrht.com', '$2y$10$rEPKIiqce4M96GkEnJmF/.jSxyqi21fkcBI2L2FaOESxwGzpqh/Ae', NULL, NULL, '2LNFxb3rObC73yo4win6wYcGb3VVWTJs', 'http://api.localhost/images/users/ff846015-04ba-396f-bd01-c353981c7e91.png', '2016-07-22 01:07:45', '2016-07-22 03:36:30', NULL, NULL),
(51, '89d46b46-e845-3416-bc4a-979b1bacd47f', 'Invitado', 'f453262@mvrht.com', '$2y$10$/aEOy.HzslGOIcU31AVA1O/c5tFDuCOAwk7EVFmfeZMIL7/J.KjJy', NULL, NULL, 'Jw6v7dUv1CAZ0wduWoIuTVWJpJcnJliS', NULL, '2016-07-22 23:05:07', '2016-07-22 23:05:07', NULL, NULL),
(52, 'e1ed227a-4a91-36f5-9a5a-530f63f86cf2', 'user demo', 'otro@server.com', '$2y$10$Ut1K9ZF3.K4gyu240BNwwuETpdImXYjS7CWGpaVKrEpCBnxpPwAGu', NULL, NULL, 'S7cXeufGJiwd397xZBbIz4JcgLufUqyc', NULL, '2016-07-23 03:53:25', '2016-07-23 03:53:25', NULL, NULL),
(53, '98ef686c-2aca-3c31-98ce-cb8aef338143', 'will', 'will@papayabus.com', '$2y$10$bdsNN9N5SJYIwDLCHpC/WuC2NJIUgepwacovle8EEB0fJfGQHEko2', NULL, NULL, '9BTlE6eAfHzlL1sNtLatDkEfshCJYWhG', NULL, '2016-08-08 23:27:36', '2016-08-08 23:27:36', NULL, NULL),
(54, '78e5dddd-5970-379b-a3c3-a60341cb1f62', 'will', 'will@papayabus1.com', '$2y$10$vU7wYpsnQC53he.xSHHT/umVqZMeY6e.Nl5Z5PtDAZ.m6jJFiLoau', NULL, NULL, 'IaX5GUTf3QsN3QG5pHQZ0jS5FQyH3Aci', NULL, '2016-08-09 00:20:35', '2016-08-09 00:20:35', NULL, NULL),
(76, '4459b2ae-2dc9-3284-98c4-d8ec660139c5', 'giomar', 'grodrim_19@gmail.com', '$2y$10$IwGRwBDzN5Pf4vRTX.ImFOuV7cs/qg392CrMp6DNC99g87FCLLk2y', NULL, NULL, 'aCePLFRY93Ctlnjlj4urBB83Pyn49uyR', NULL, '2016-12-12 23:33:25', '2016-12-12 23:33:25', NULL, NULL),
(77, '51dfc83c-5df6-3c21-b363-0f30e70f69aa', 'giomar', 'groddrim_19@gmail.com', '$2y$10$c0t2qM7yXtrQbBAqHJW38uxROi5s0JpL0tNpq5QXKqHL2Jbu45Wv6', NULL, NULL, 'TbdpJWmlwJHttXcXnM2oH0tswJownzYR', NULL, '2016-12-13 00:02:09', '2016-12-13 00:02:09', NULL, NULL),
(78, 'fdc18cdb-d3af-3afe-8b53-ae7e89ecfbaa', 'giomar Rodríguez', 'antonio@gmail.com', '$2y$10$hPb83Rl9sJ21CxK6Zlr5zeo1SjZkdx14kxVHo9JVZd.N9pXODIB4S', NULL, NULL, 'I2nZTXy97kJLikOPz6SD11qFdh9DjilS', NULL, '2016-12-13 00:14:31', '2016-12-13 00:14:31', NULL, NULL),
(79, '8363e333-59dd-3573-b025-8224c9bee073', 'antonio miranda', 'miranda@gmail.com', '$2y$10$RzBf34PiGxztnWGlPiJUuu6M8kvFdxKIrIRxSafLPsTWSr7jqOFWO', NULL, NULL, 'Brzoznn2wnb0Ae3tCct1QGCC1DAFWhc0', NULL, '2016-12-13 00:17:13', '2016-12-13 00:17:13', NULL, NULL),
(80, '65d96a6e-c239-3a61-955a-8a5759ab4975', 'giomar', 'groddraim_19@gmail.com', '$2y$10$l6yB.Ezzw2xXUjZOiO1TEuRjvykSr0TkjyLqghGiYjEX7.JvpzjFe', NULL, NULL, 'FhVARYCWIuN4SLvhlCj3lo8qBPuXibLE', NULL, '2016-12-13 00:17:40', '2016-12-13 00:17:40', NULL, NULL),
(81, 'fee3e1cf-cb6b-3d12-9b9b-7e0f1b99a322', 'antonio giomar', 'giomar1@gmail.com', '$2y$10$GNLOT9D/JN58sSLqbrIYwOKvwfbN8JCKo4b1Ll5VqJMk6HANkDZk2', NULL, NULL, 'q4l4GejiqKlfejdmC4LmEYWbIzydX8Gd', NULL, '2016-12-13 00:18:53', '2016-12-13 00:18:53', NULL, NULL),
(82, 'ca75a3c0-ffaf-3001-9982-c1952d8e4a4c', 'giomar', 'grodddraim_19@gmail.com', '$2y$10$csPbtG3uVy2yAZ2HJdIvz.krEvLXS3GzQqKcW0TDua5S1gb3MEnX2', NULL, NULL, 'ZAGKQiBFResCNyilpUAIrK2ZFkDSGZjy', NULL, '2016-12-13 00:20:40', '2016-12-13 00:20:40', NULL, NULL),
(83, '95520e1a-cfd5-3516-bbd6-ddc03f6a97d9', 'giomar rodriguez', 'giomar2@gmail.com', '$2y$10$ih5I6CEmY2go1FVAukcuxe/mWcRfnWQZbVfx5lTRA6vk8NosWCEfu', NULL, NULL, '8MYWILsZjDblZq2hO7E0V0BPUR4of0It', NULL, '2016-12-13 00:23:03', '2016-12-13 00:23:03', NULL, NULL),
(84, '8e712158-8af5-3439-a6d7-0ce320ed9e56', 'giomar', 'grofdddraim_19@gmail.com', '$2y$10$RbCwGNwR8vYyMMYx1Z2Kh.BswgmDyZ78u5cAAIqI3rRMQpeY7Vd6i', NULL, NULL, 'czkVV4cgLNxAl8ydONt7BRQ92cwCz2wt', NULL, '2016-12-13 00:23:54', '2016-12-13 00:23:54', NULL, NULL),
(85, 'e97e9509-9322-352f-ac8b-d23fa19d98f7', 'giomar', 'grofdgddraim_19@gmail.com', '$2y$10$42neaJx.Tx/RsDxY8KCd0.61eYGmRNX1ZXxvA7dUYM7oooN/cZkGC', NULL, NULL, 'vLEWEWcyqeP7TxHbY639NaFb0uMaOEKh', NULL, '2016-12-13 00:26:29', '2016-12-13 00:26:29', NULL, NULL),
(86, '63b3e01d-7e84-3564-a29a-b73d08f90eab', 'giomar', 'grofdgkddraim_19@gmail.com', '$2y$10$b6VKooWit1nuiRPLa/4Hbe5hik6dT8a2P97LP5bzk6SpxapkBd4a6', NULL, NULL, 'f8NzeRJqp6rjHs8P0jJyecM1thOFjaxn', NULL, '2016-12-13 00:32:53', '2016-12-13 00:32:53', NULL, NULL),
(87, '1b104076-85a0-3491-9643-e4c571e3a1e5', 'giomar antonio', 'giomar3@gmail.com', '$2y$10$kUispm2PBn8cLQYCGLECT.kuUE97G/FqolGAnvJmNRs2VzudLTcCK', NULL, NULL, 'GGCNdWlcQkOhSLNV0tgtN5f0ZPsE0wsd', NULL, '2016-12-13 00:46:30', '2016-12-13 00:46:30', NULL, NULL),
(88, '4e9c0278-c552-3af0-81f6-3389be02a82d', 'willy Estrada', 'willy@cinepapaya.com', '$2y$10$KUfwjVVsjGVTHj4nPxObnus8SiV7A8y8QaeXYMxNt0x.mhNNSwXvG', NULL, NULL, '49spk1zLAfR7nRRYs4LZDPQobnUVFH6P', NULL, '2016-12-14 00:01:48', '2016-12-14 00:01:48', NULL, NULL),
(89, 'a98525da-d8a2-316e-a422-ae22fafbf3ff', 'giomar rodriguez', 'giomar@gmail.com', '$2y$10$sCW.z9GR74LC64nPCyq8k.7TSGCjmkNq4/XDo9w3jQ1XB1EsodPAe', NULL, NULL, 'VG9nz3H9ESa78CkwdKKSOhKkO6R3w45w', NULL, '2016-12-14 01:14:09', '2016-12-14 01:14:09', NULL, NULL),
(90, '5aa38287-7a73-3974-a022-ae0d60a647ae', 'elvis renzo', 'renzo@gmail.com', '$2y$10$ZzNgXOqXCP88oxihhWDzE.m3hGZSa5DL8Qq0o8HR0LSyR/93n79aS', NULL, NULL, 'N98ZIMUB8pkeu90NsszzRhcqSURJ9li9', NULL, '2016-12-14 01:31:19', '2016-12-14 01:31:19', NULL, NULL),
(91, '38324be5-44ee-3347-bce1-c4446f560d94', 'ana belen', 'ana@gmail.com', '$2y$10$eVKe2AcaqM7OfCHbyqKRtuuIazvbZbYdqgabVf..ZJv1lQohS071W', NULL, NULL, 'TzlpE0eYFtpb3FNA0FlqKsssCBENIvCC', NULL, '2016-12-14 01:32:33', '2016-12-14 01:32:33', NULL, NULL),
(92, '9f06d52c-e359-3e52-b70a-0ef80f176c50', 'giomar antonio', 'an@gmail.com', '$2y$10$QyHQk/VN0VB4r3Lcp2gKOutnOnM6xAU3MftEyzscDSHztM2sNpFuq', NULL, NULL, 'bQ0jAmMvJcTScrvsq3RM5cuFsNjPxweI', NULL, '2016-12-14 01:33:46', '2016-12-14 01:33:46', NULL, NULL),
(93, '912a3fac-60aa-3930-9ffe-a5fe64aa1990', 'giomar miranda', 'ant@gmail.com', '$2y$10$b2BZ0C5P//mD7s88PxKOTuRa1XVW9K6jmToVeOg7lEm0vTgBUySda', NULL, NULL, 't3ioT042freG1h2oHHDAZp6IeKDRRmkS', NULL, '2016-12-14 01:38:43', '2016-12-14 01:38:43', NULL, NULL),
(94, '1b1779a8-4bc6-3dd0-900a-92140922e4a0', 'antonio miranda', 'miranda2@gmail.com', '$2y$10$EEfAkxd4pZIVUzj6N4Mje.eXTNx8CymaLTLCI1sZpW5anFqX32JSa', NULL, NULL, 'Ze4wl3VNGS5CXWTp76vAmREkT6FFn4VV', NULL, '2016-12-14 01:48:55', '2016-12-14 01:48:55', NULL, NULL),
(95, 'c82b787d-2686-3e25-abcb-276d4646010d', 'cesar rodriguez', 'cesar@gmail.com', '$2y$10$B5BbFKfEb95kjLMOt.DMs.w46yRy/JmGUTWDHH7kU8kv6fsRBeeiC', NULL, NULL, 'RZ6JquAVthu1BlvLt8N8hWEN4g7ktzTi', NULL, '2016-12-14 01:51:53', '2016-12-14 01:51:53', NULL, NULL),
(96, 'd87c1766-fea5-3ac5-a3db-33047ee6bd3d', 'cesar rodriguez', 'cesar@hotmail.com', '$2y$10$ggemWvCUTsCdkByPXE79Iu1oaxZv2T2hrMKy1xVq63yJklyZbPMu.', NULL, NULL, 'fh5J3qFug0JQdt1F9n3jFw2MyNrB5KOn', NULL, '2016-12-14 01:55:19', '2016-12-14 01:55:19', NULL, NULL),
(97, '8076d2c9-c1df-3947-ba6f-1e9cde562746', 'giomar', 'anton@gmail.com', '$2y$10$r21f2NxIQi0LUQjvYLawg.AtBKfEK1DOBfCjv/gnh20vB1zGPKnMW', NULL, NULL, 'ywPTaS29Y0eWzg1E5jm9PT6NqzCkvmTy', NULL, '2016-12-16 00:29:10', '2016-12-16 00:29:10', NULL, NULL),
(98, 'edd8c6fc-d16b-3e8e-a80f-c18fca4f055c', 'cesar', 'cesaron@hotmail.com', '$2y$10$yeAfofsGbB7p8fEKhs01O.JA9ace0Ale3kmxlVkMfkiwF1jkSA3Ba', NULL, NULL, '72WowKEGXAziIcgcuc91s4wW53eTAkUl', 'http://192.168.11.182/api.papayafast.com/public/images/users/d1c12c30-19f0-34ac-a8b4-0932651a9d48.png', '2016-12-16 20:41:30', '2016-12-16 20:43:59', NULL, NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `uuid` (`uuid`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=99;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
